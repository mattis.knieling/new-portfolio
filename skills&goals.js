document.addEventListener('DOMContentLoaded', () => {

//------------ Get Skills and Goals button -----------\\

const skillsButton = document.getElementById('skills-button');
const goalsButton = document.getElementById('goals-button');



//------------ Get all Skill bars -----------\\
const htmlSkills = document.getElementById('html-skills');
const cssSkills = document.getElementById('css-skills');
const jsSkills = document.getElementById('js-skills');
const vueSkills = document.getElementById('vue-skills');
const pySkills = document.getElementById('py-skills');
const csharpSkills = document.getElementById('csharp-skills');
const gitSkills = document.getElementById('git-skills');
const wpSkills = document.getElementById('wp-skills');
const ubuntuSkills = document.getElementById('ubuntu-skills');
const photoshopSkills = document.getElementById('photoshop-skills');
const msoSkills = document.getElementById('mso-skills');
const sqlSkills = document.getElementById('sql-skills');

//------------ Get all Goal bars -----------\\

const htmlGoals = document.getElementById('html-goals');
const cssGoals = document.getElementById('css-goals');
const sassGoals = document.getElementById('sass-goals');
const lessGoals = document.getElementById('less-goals');
const jsGoals = document.getElementById('js-goals');
const tsGoals = document.getElementById('ts-goals');
const vueGoals = document.getElementById('vue-goals');
const reactGoals = document.getElementById('react-goals');
const pyGoals = document.getElementById('py-goals');
const csharpGoals = document.getElementById('csharp-goals');
const javaGoals = document.getElementById('java-goals');
const gitGoals = document.getElementById('git-goals');
const wpGoals = document.getElementById('wp-goals');
const ubuntuGoals = document.getElementById('ubuntu-goals');
const photoshopGoals = document.getElementById('photoshop-goals');
const msoGoals = document.getElementById('mso-goals');
const sqlGoals = document.getElementById('sql-goals');


//-------- Show Skills & Goals at click --------\\\

//------------- Skills ------------------\\

skillsButton.addEventListener('click', function(){

    htmlSkills.style.width = '90%' ;
    cssSkills.style.width = '90%' ;
    jsSkills.style.width = '30%' ;
    vueSkills.style.width = '30%' ;
    pySkills.style.width = '25%' ;
    csharpSkills.style.width = '15%' ;
    gitSkills.style.width = '60%' ;
    wpSkills.style.width = '50%' ;
    ubuntuSkills.style.width = '65%' ;
    photoshopSkills.style.width = '55%' ;
    msoSkills.style.width = '80%' ;
    sqlSkills.style.width = '45%' ;
  
  
  });
  
  
  //------------- Goals ----------------\\
  
  
  goalsButton.addEventListener('click', function(){
  
    htmlGoals.style.width = '100%' ;
    cssGoals.style.width = '100%' ;
    sassGoals.style.width = '100%' ;
    lessGoals.style.width = '100%' ;
    jsGoals.style.width = '100%' ;
    tsGoals.style.width = '100%' ;
    vueGoals.style.width = '80%' ;
    reactGoals.style.width = '100%' ;
    pyGoals.style.width = '100%' ;
    csharpGoals.style.width = '100%' ;
    javaGoals.style.width = '100%' ;
    gitGoals.style.width = '100%' ;
    wpGoals.style.width = '80%' ;
    ubuntuGoals.style.width = '100%' ;
    photoshopGoals.style.width = '100%' ;
    msoGoals.style.width = '80%' ;
    sqlGoals.style.width = '100%' ;
  
  
  
  });
})