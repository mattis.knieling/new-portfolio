
document.addEventListener('DOMContentLoaded', () => {

  //----- Home Page Animation ---------\\
    anime({
        targets: 'nav a',
        translateY: [-150, 0],
        easing: 'easeOutExpo',
        delay: (el, i) =>{
            return 300 + 100 *i
        },
        opacity: [0, 1],
        duration: 1000,
      });
    anime({
        targets: '.home-content',
        duration: 3000,
        easing: 'easeOutExpo',
        translateX: [250, 0],
        delay: 1500,
        opacity: [0, 1]
      });
    anime({
        targets: '.name',
        duration: 4000,
        easing: 'easeOutExpo',
        translateX: [-250, 0],
        delay: 1000,
        opacity: [0, 1]
    });
    anime({
        targets: '.main-img',
        duration: 5000,
        easing: 'easeOutExpo',
        opacity: [0, 1],
        delay: 1500,
        opacity: [0, 1]
    });
    
//----- Contact Page Animation ---------\\
    anime({
        targets: '.infos p',
        translateY: [150, 0],
        easing: 'easeOutExpo',
        delay: (el, i) =>{
            return 1000 + 100 *i
        },
        opacity: [0, 1],
        duration: 1000,
      });
      anime({
        targets: '.infos h1',
        translateY: [150, 0],
        easing: 'easeOutExpo',
        delay:1000, 
        opacity: [0, 1],
        duration: 1000,
      });
      anime({
        targets: '.infos ',
        easing: 'easeOutExpo',
        delay:  1000,
        opacity: [0, 1],
        duration: 8000,
      });
      anime({
        targets: '.form label',
        translateY: [150, 0],
        easing: 'easeOutExpo',
        delay: (el, i) =>{
            return 1000 + 500 *i
        },
        opacity: [0, 1],
        duration: 3000,
      });
      anime({
        targets: '.form input',
        width: [0 , 400],
        easing: 'easeOutExpo',
        delay: (el, i) =>{
            return 1000 + 100 *i
        },
        opacity: [0, 1],
        duration: 4000,
      })
      anime({
        targets: '.form textarea',
        width: [0 , 400],
        easing: 'easeOutExpo',
        delay:  1500 ,
        opacity: [0, 1],
        duration: 4000,
      })
      anime({
        targets:'.sendMail',
        width: [0 , 420],
        easing: 'easeOutExpo',
        delay:  1500 ,
        opacity: [0, 1],
        duration: 4000,

      })
//----- Skills & Goals Page Animation ---------\\      
        anime({
          targets: '.skills',
          translateX: [-250, 0],
          easing: 'easeOutExpo',
          delay:  500 ,
          opacity:[0, 1],
          duration: 2000,
        })
        anime({
          targets: '.goals',
          translateX: [250, 0],
          easing: 'easeOutExpo',
          delay:  500 ,
          opacity:[0, 1],
          duration: 2000,
        })
//----- About Me Page Animation ---------\\
      anime({
        targets: '.content-aboutMe p',
        translateY: [250, 0],
        easing: 'easeOutExpo',
        delay: (el, i) =>{
            return 1000 + 100 *i
        },
        opacity:[0, 1],
        duration: 2500,
      })
      anime({
        targets: '.content-aboutMe h1',
        translateX: [250, 0],
        easing: 'easeOutExpo',
        delay:  500 ,
        opacity:[0, 1],
        duration: 2000,
      })
})